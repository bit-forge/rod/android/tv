package com.bit_forge.rod.models

data class MainData(
    val id: Int,
    val title: String,
    val description: String
) {
//    fun menuItems() = listOf(
//        "Home" to "Home",
//        "Settings" to "Settings"
//    )

    public final fun current() : MainData{
        return MainData(1, "Home", "Home")
    }

    fun menuItems() : List<MenuItemData> {
        return MenuItemData.all()
    }

}
