package com.bit_forge.rod.models

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Settings

data class MenuItemData(
    val id: Int,
    val title: String,
    val description: String,
    val icon: String
) {
    fun imageVector() = when (icon) {
        "Home" -> Icons.Default.Home
        "Settings" -> Icons.Default.Settings
        "Favourites" -> Icons.Default.Favorite
        else -> Icons.Default.Home
    }

    companion object {
        fun all(): List<MenuItemData> {
            return listOf(
                MenuItemData(1, "Home", "Home", "Home"),
                MenuItemData(2, "Settings", "Settings", "Settings"),
                MenuItemData(3, "Favourites", "Favourites", "Favourites")
            )
        }
    }
}
