package com.bit_forge.rod.views.layout

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.tv.material3.ExperimentalTvMaterial3Api
import com.bit_forge.rod.models.MenuItemData

@OptIn(ExperimentalTvMaterial3Api::class)
@Preview(device = "id:Android TV (720p)", backgroundColor = 0xFFFFFFFF)
@Composable
fun AppMainMenu(menuWidth : Dp = 80.dp) {
    var selectedIndex by remember { mutableIntStateOf(0) }
    val menuItems = MenuItemData.all()

    Column(
        modifier = Modifier
            .fillMaxHeight()
//            .width(menuWidth)

            .background(Color.Gray)
            .selectableGroup(),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.spacedBy(10.dp)
    ) {
        menuItems.forEachIndexed { index, menuItemData ->
            AppMainMenuItem(
                selectedIndex = selectedIndex,
                index = index,
                menuItemData = menuItemData,
                onMenuItemSelected = { selectedIndex = it }
            )
        }
//        Text(text = "Home")

//        NavigationDrawerItem(
//            selected = selectedIndex == index,
//            onClick = { selectedIndex = index },
//            leadingContent = {
//                Icon(
//                    imageVector = icon,
//                    contentDescription = null,
//                )
//            }
//        ) {
//            Text(text)
//        }
    }
}

