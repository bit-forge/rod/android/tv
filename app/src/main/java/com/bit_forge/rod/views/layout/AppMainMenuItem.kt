package com.bit_forge.rod.views.layout

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.tv.material3.ExperimentalTvMaterial3Api
import androidx.tv.material3.Icon
import androidx.tv.material3.Text
import androidx.tv.material3.NavigationDrawerItem
import com.bit_forge.rod.models.MenuItemData

@OptIn(ExperimentalTvMaterial3Api::class)
@Composable
fun AppMainMenuItem (
    selectedIndex: Int,
    index: Int,
    menuItemData: MenuItemData,
    onMenuItemSelected: (Int) -> Unit
) {
//    NavigationDrawerItem(
//        selected = selectedIndex == index,
//        onClick = onMenuItemSelected,
//        leadingContent = {
//            Icon(
//                imageVector = menuItemData.imageVector(),
//                contentDescription = null,
//            )
//        },
//        content = {
//            Text(menuItemData.title)
//        }
//    )
}