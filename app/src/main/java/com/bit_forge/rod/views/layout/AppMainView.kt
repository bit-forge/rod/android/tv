package com.bit_forge.rod.views.layout

import android.content.res.Resources.Theme
import android.widget.Space
import androidx.compose.foundation.background
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import androidx.tv.foundation.lazy.list.TvLazyRow
import androidx.tv.material3.Button
import androidx.tv.material3.Card
import androidx.tv.material3.DrawerState
import androidx.tv.material3.DrawerValue
import androidx.tv.material3.ExperimentalTvMaterial3Api
import androidx.tv.material3.Icon
import androidx.tv.material3.MaterialTheme
import androidx.tv.material3.NavigationDrawer
import androidx.tv.material3.NavigationDrawerItem
import androidx.tv.material3.Surface
import androidx.tv.material3.Text
import com.bit_forge.rod.models.MenuItemData
import androidx.tv.material3.rememberDrawerState

@OptIn(ExperimentalTvMaterial3Api::class)
@Preview(device = "id:Android TV (720p)")
@Composable
fun AppMainView() {
    var selectedIndex by remember { mutableIntStateOf(0) }
    val menuItems = MenuItemData.all()
    val drawerState = rememberDrawerState(DrawerValue.Closed)

//    val navController = rememberNavController()

    NavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .background(MaterialTheme.colorScheme.primary)
                    .padding(10.dp)
//                    .selectableGroup(),
                        ,
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.spacedBy(15.dp)
            ) {
                menuItems.forEachIndexed { index, menuItemData ->
                    NavigationDrawerItem(
                        modifier = Modifier.onFocusChanged { focusState ->
                            if (focusState.isFocused) {
                                selectedIndex = index
                            }
                        },
                        selected = selectedIndex == index,
                        onClick = { selectedIndex = index },
                        leadingContent = {
                            Icon(
                                imageVector = menuItemData.imageVector(),
                                contentDescription = null,
                            )
                        },
                        content = {
                            Text(menuItemData.title)
                            Text(text = "$index")
                        }
                    )
                }
            }
        }
//        scrimBrush = scrimBrush
    ) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
//                .padding(start = 10.dp)
                .background(Color.White)
                .focusable(false)
        ) {


            Text(text = "Hello World $selectedIndex")

//            LazyColumn(
//                modifier = Modifier.padding(10.dp)
//            ) {
//                items(10) {
//                    Card(onClick = { /*TODO*/ }) {
//                        Text(text = "Card")
//                    }
//                }
//            }
//            Row (
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .padding(start = 10.dp),
//                horizontalArrangement = Arrangement.spacedBy(10.dp)
//
//
//            ) {
//                Button(onClick = {}) {
//                    Text(text = "Open Drawer")
//                }
//                Button(onClick = {}) {
//                    Text(text = "Open Drawer")
//                }
//
//                Button(onClick = {}) {
//                    Text(text = "Open Drawer")
//                }
//            }


        }
    }
}
